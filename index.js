/**
 * /**
 * @file Archivo que contiene el módulo ova-concepts
 * @namespace index
 * @module ova-concepts
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 */

const $ = require('jquery');
const _ = require('lodash');
const swal = require('sweetalert2');
const stageResize = require('stage-resize');
const presentation = require('presentation');
/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {
    currentProps: {
        amount: 0,
        enabled: true,
        concepts: {}
    },
    render: function(args){
        if(!args.$target){
            throw new Error("A target is expected.");
        }
        if(!args.$target instanceof jQuery){
            throw new Error("Invalid target. A jQuery element was expected.");
        }
        if(!args.$target.length){
            throw new Error("jQuery target not available in DOM.");
        }

        const template = require('templates/navigationButtons.hbs');
        return $(template())
                .appendTo(args.$target);
    },
    initializeNavigationButtons: function(args){
        const $el = module.exports.render(args);
        $el.find('.prev')
        .on('click', function(){
            presentation.switchToSlide({
                slide: 'prev'
            });

        })
        .end()
        .find('.home')
        .on('click', function(){
            presentation.switchToSlide({
                slide: 'home'
            });
        })
        .end()
        .find('.next')
        .on('click', function(){
            presentation.switchToSlide({
                slide: 'next'
            });
        })
        .end()
        .children().on('click', function(){
            if($(this).hasClass('highlight')){
                $(this).removeClass('highlight');
            }
        });
    }
};